<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/toto','MainController@index')->name('toto');
Route::get('/participant','ParticipantController@index')->name('participant');
Route::get('/participant/create','ParticipantController@create')->name('participant.create');
Route::post('/participant/create','ParticipantController@store')->name('participant.store');
Route::get('/participant/edit/{id}','ParticipantController@edit')->name('participant.edit');
Route::put('/participant/update','ParticipantController@update')->name('participant.update');

//Route::resource('/participant','MainController');
Route::resource('/tache','TacheController');