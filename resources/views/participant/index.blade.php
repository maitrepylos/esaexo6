@extends('layout.app')

@section('content')

    <?php dd($participant) ?>

    <table class="table table-responsive">

        <tr>
            <th>Nom</th>
            <th>Prenom</th>
            <th>age</th>
        </tr>
        @foreach($participant as $value)
            <tr>
                <td>{{$value->nom}}</td>
                <td>{{$value->prenom}}</td>
                <td>{{$value->age->format('d-m-Y')}}</td>
            </tr>
        @endforeach

    </table>



@endsection()