@extends('layout.app')

@section('content')

    <div class="container">
        <section class="row">

            <form action="{{route('participant.update')}}" method="put">

                @csrf

                @include('participant.fields')


            </form>


        </section>

    </div>
@endsection()