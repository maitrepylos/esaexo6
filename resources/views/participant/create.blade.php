@extends('layout.app')

@section('content')

<div class="container">    <section class="row">

        <form action="{{route('participant.store')}}" method="post">

            @csrf

          @include('participant.fields')


        </form>



    </section>

</div>
@endsection()