<div class="form-group">
    <label for="nom">
        Nom:
    </label>
    <input type="text" name="nom" value="{{old('nom',$nom)}}">
</div>
<div class="form-group">
    <label for="prenom">
        Pr&eacute;nom:
    </label>
    <input type="text" name="prenom" value="{{old('prenom',$prenom)}}">
</div>
<div class="form-group">
    <label for="age">
        Age:
    </label>
    <input type="date" name="age" value="{{old('age',$age->format('Y-m-d'))}}">
</div>

@if($id != null)
    <input type="hidden" name="id" value="{{$id}}" />
@endif

<input type="submit">