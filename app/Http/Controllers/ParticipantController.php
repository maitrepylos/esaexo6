<?php

namespace App\Http\Controllers;

use App\Http\Requests\ParticipantRequest;
use App\Models\Participant;
use Illuminate\Http\Request;

class ParticipantController extends Controller
{

    public function index(){
        $participant = Participant::find(1);
        dd($participant->majuscule);
        return view('participant.index',compact('participant'));
    }

    public function create()
    {
        $participant = new Participant;
        $participant->nom = '';
        $participant->prenom = '';
        $participant->age = '';
        return view('participant.create',$participant);
    }

    public function store(ParticipantRequest $request)
    {
        $data = $request->except(['_token']);
        $participant = Participant::create($data);


       return redirect(route('participant.create'));



    }

    public function edit($id)
    {
        $participant = Participant::find($id);
        return view('participant.edit',$participant);

    }

    public function update(ParticipantRequest $request)
    {
        $p = Participant::find($request->id);
        $data = $request->except(['_token']);
        $p->update($data);

        redirect()->route('participant.edit',$request->id);

    }

}
