<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    protected $table = 'participant';
//    protected $primaryKey = 'id';
//    public $timestamps = false;
//    public const CREATED_AT = 'creation';
//    public const UPDATED_AT = 'update';
    protected $fillable = ['nom', 'prenom', 'age'];


    public function getAgeAttribute()
    {

        $date = new Carbon($this->attributes['age']);
        return $date;

    }

    public function getMajusculeAttribute()
    {

        return strtoupper($this->attributes['nom']) . ' ' . $this->attributes['prenom'];
    }


}
